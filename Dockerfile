FROM node:latest AS build
WORKDIR /usr/src/app

#RUN npm cache clean
#RUN npm install
#RUN npm audit fix0
#RUN npm run start

COPY package*.json ./
COPY ./ /usr/src/app

EXPOSE 3000
CMD ["node", "server.js"]

FROM nginx:latest
#
#comments
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf

WORKDIR /usr/src/app
COPY --from=build /usr/src/app /usr/share/nginx/html

EXPOSE 5000
#CMD ["nginx", "-g","daemon off"]
CMD ["node", "server.js"]